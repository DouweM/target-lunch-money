import singer
import logging
import requests
import re
from decimal import Decimal
from typing import Dict, List

logger = singer.get_logger()

class LunchMoney:
    BASE_URL = "https://dev.lunchmoney.app/v1/"

    def __init__(self, token: str):
        self.token = token

    @property
    def _headers(self):
        return {"Authorization": f"Bearer {self.token}"}

    def _get(self, path: str) -> requests.Response:
        response = requests.get(
            f"{self.BASE_URL}{path}",
            headers=self._headers,
        )
        response.raise_for_status()
        return response

    def _put(self, path: str, payload: Dict):
        response = requests.put(
            f"{self.BASE_URL}{path}",
            headers=self._headers,
            json=payload,
        )
        response.raise_for_status()
        if "errors" in response.json():
            raise Exception(str(response.json()["errors"]))

        return response

    def list_assets(self) -> List[Dict]:
        return self._get("assets").json()["assets"]

    def update_asset(self, asset: Dict, new_balance: Decimal = None, new_price: Decimal = None, new_quantity: Decimal = None):
        logger.debug(f"asset: {asset}")

        id = asset["id"]
        current_balance = Decimal(asset["balance"])

        current_name = asset["name"]
        metadata_match = re.search(r'^(.*) \(([a-z0-9_,.: ]+)\)$', current_name)
        metadata = {
            k: Decimal(v)
            for (k, v)
            in (
                pair.split(': ')
                for pair
                in metadata_match[2].split(", ")
            )
        } if metadata_match else {}
        current_quantity = metadata.get("q", metadata.get("quantity"))
        current_price = metadata.get("p", metadata.get("price"))

        # Either or both of price and quantity
        if new_price is not None or new_quantity is not None:
            # Derive balance from quantity and price (new, current, or default)
            new_quantity = new_quantity or current_quantity or Decimal(1)
            new_price = new_price or current_price or current_balance or Decimal(0)
            new_balance = new_price * new_quantity
        # Or just a balance
        elif new_balance is not None:
            # Derive price from balance and (new) quantity
            new_quantity = current_quantity or Decimal(1)
            new_price = new_balance / new_quantity

        new_name = metadata_match[1] if metadata_match else current_name
        if new_quantity != 1 and new_price != new_balance:
            metadata = {
                "q": new_quantity,
                "p": new_price
            }

            logger.debug(f"metadata: {metadata}")

            new_name += " (" + ", ".join(f"{k}: {v:.4f}" for (k, v) in metadata.items() if v is not None) + ")"

        updates = {
            "name": new_name,
            "balance": f"{new_balance:.4f}"
        }

        logger.debug(f"updates: {updates}")

        self._put(f"assets/{id}", updates)

        return {**asset, **updates}
