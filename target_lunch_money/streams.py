import singer
import decimal

from .singer_sdk.stream import MappingStream
from .lunch_money import LunchMoney

logger = singer.get_logger()


class TargetLunchMoneyStream(MappingStream):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.lm = LunchMoney(self.target.config["access_token"])


class AssetsStream(TargetLunchMoneyStream):
    name = "assets"

    schema = {
        "name": {"type": "string"},
        "balance": {"type": "number"},
        "price": {"type": "number"},
        "quantity": {"type": "number"},
    }

    def persist_record(self, record):
        logger.debug(f"record: {record}")

        name = record["name"]
        balance = record.get("balance")
        balance = decimal.Decimal(balance) if balance is not None else None
        price = record.get("price")
        price = decimal.Decimal(price) if price is not None else None
        quantity = record.get("quantity")
        quantity = decimal.Decimal(quantity) if quantity is not None else None

        try:
            # TODO: Support other ways of matching than "substring"
            lm_asset = next(
                lm_asset
                for lm_asset in self.lm.list_assets()
                if name in lm_asset["name"]
            )
        except StopIteration:
            message = "Asset with name '{}' not found".format(name)
            if balance or (price and quantity):
                raise Exception(message)
            else:
                logger.error(message)
                return

        logger.debug(f"lm_asset: {lm_asset}")

        current_balance = lm_asset["balance"]
        lm_asset = self.lm.update_asset(lm_asset,
                                        new_balance=balance,
                                        new_price=price,
                                        new_quantity=quantity)
        logger.info(
            "Updated '{}' balance from {} to {}".format(
                lm_asset["name"], current_balance, lm_asset["balance"]
            )
        )
